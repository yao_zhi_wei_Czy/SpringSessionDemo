package com.yao.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import com.yao.interceptor.LoginInterceptor;

@Configuration
public class WebInterceptor extends WebMvcConfigurerAdapter {
	@Autowired
	private LoginInterceptor loginInterceptor;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// TODO Auto-generated method stub
		//添加登陆拦截器
		registry.addInterceptor(loginInterceptor).addPathPatterns("/**").excludePathPatterns("/**/*Login*/**").excludePathPatterns("/**/*login*/**");
	}

}
