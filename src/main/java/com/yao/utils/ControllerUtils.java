package com.yao.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ControllerUtils {
	public static String getCookies(HttpServletRequest request, String name) throws UnsupportedEncodingException {
		Cookie[] cookies = request.getCookies();
		if (cookies == null) {
			return "";
		}
		for (Cookie cookie : cookies) {
			if (name.equals(cookie.getName()))
				return URLDecoder.decode(cookie.getValue(), "UTF-8");
		}
		return "";
	}

	public static void setCookie(HttpServletResponse response, String name, String value)
			throws UnsupportedEncodingException {
		Cookie cookie = new Cookie(name, URLEncoder.encode(value, "UTF-8"));
		cookie.setPath("/");
		response.addCookie(cookie);
	}
}
