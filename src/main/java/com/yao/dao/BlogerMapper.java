package com.yao.dao;

import com.yao.entity.Bloger;
import tk.mybatis.mapper.common.Mapper;

public interface BlogerMapper extends Mapper<Bloger> {
}