package com.yao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yao.dao.BlogerMapper;
import com.yao.entity.Bloger;

@Transactional
@Service
public class BlogerService {
	@Autowired
	private BlogerMapper blogerMapper;

	public BlogerMapper getBlogerMapper() {
		return blogerMapper;
	}

	public Bloger selectBlogerByAccountAndPassword(String account, String password) {
		Bloger criteria = new Bloger();
		criteria.setAccount(account);
		criteria.setPassword(password);
		return blogerMapper.selectOne(criteria);
	}
}