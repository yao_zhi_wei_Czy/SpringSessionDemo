package com.yao.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String home() {
		return "practice/home";
	}

	@RequestMapping("/openWork/{work}")
	public String openWork(@PathVariable("work") String work) {
		return "practice/" + work;
	}

	/**
	 * @return 拦截进入cookieSessionLogin
	 */
	@RequestMapping("/cookieSessionLogin")
	public String cookieSessionLogin() {
		return "practice/cookieSessionLogin";
	}
}