package com.yao.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.yao.entity.Bloger;
import com.yao.service.BlogerService;

@Controller
@RequestMapping("/yao/Bloger")
public class BlogerController {
	@Autowired
	private BlogerService blogerService;

	@ResponseBody
	@RequestMapping("/loginBlogerByNameAndPassword")
	public String loginBlogerByNameAndPassword(HttpServletResponse response,HttpServletRequest request, String account, String password)
			throws IOException {
		Bloger bloger = blogerService.selectBlogerByAccountAndPassword(account, password);
		if(bloger==null)
			return "failed";
		request.getSession().setAttribute("bloger", bloger);
		request.getSession().setMaxInactiveInterval(30*60);
		return "success";
	}
	
	@ResponseBody
	@RequestMapping("/loginoutBloger")
	public String loginoutBloger(HttpServletResponse response,HttpServletRequest request, String account, String password)
			throws IOException {
		System.out.println("被访问了");
		request.getSession().removeAttribute("bloger");
		return "success";
	}
}