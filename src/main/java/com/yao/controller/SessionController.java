package com.yao.controller;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/yao/session")
public class SessionController {

	@ResponseBody
	@RequestMapping(value = "/get")
	public Map<String, Object> getSession(HttpServletRequest request) {
		HttpSession session = request.getSession();
		Map<String, Object> map = new HashMap<>();
		map.put("sessionId", session.getId());
		return map;
	}
}
